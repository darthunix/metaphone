import re


class Lexeme:
    def __init__(self, body):
        """

        :type body: str
        """
        self.body = body.lower().strip()
        # Правила замены гласных
        self._vowels = {"(?:йо|ио|йе|ие)": "и", "[оыя]": "а", "[еёэ]": "и", "[ю]": "у"}
        # Правила оглушения согласных
        self._consonants = {"б": "п", "з": "с", "д": "т", "в": "ф"}
        # Согласная должна быть оглушена, если за ней следует один из символов списка _deafening_chars
        self._deafening_chars = ["б", "в", "г", "д", "ж", "з", "й", "^"]

    def _remove_double_chars(self):
        return Lexeme("".join((char for num, char in enumerate(self.body) if char != self.body[num - 1])))

    def _deafen_consonants(self):
        modified_body = ""
        for num, char in enumerate(self.body):
            if char in self._consonants and (self.body[num + 1] in self._deafening_chars or num == len(self.body) - 1):
                modified_body += self._consonants[char]
            else:
                modified_body += char
        return Lexeme(modified_body)

    def _replace_vowels(self):
        body = self.body
        for item in self._vowels:
            body = re.sub(item, self._vowels[item], body)
        return Lexeme(body)

    def metaphone(self):
        return self._replace_vowels()._deafen_consonants()._remove_double_chars().body


if __name__ == "__main__":
    lex = Lexeme("Ваааазген")
    print(lex.metaphone())
